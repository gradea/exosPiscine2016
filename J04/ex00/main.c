/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: igradea <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/03 17:36:48 by igradea           #+#    #+#             */
/*   Updated: 2016/09/03 22:50:21 by igradea          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_iterative_factorial(int nb);
void	ft_putchar(char c);

int		main(void)
{
	int fact3;
	int fact_10;
	int fact0;
	int fact12;
	int fact13;

	fact3 = ft_iterative_factorial(3);
	fact_10 = ft_iterative_factorial(-10);
	fact0 = ft_iterative_factorial(0);
	fact12 = ft_iterative_factorial(12);
	fact13 = ft_iterative_factorial(13);
	printf("%d - ", fact3);
	printf("%d - ", fact_10);
	printf("%d - ", fact0);
	printf("%d - ", fact12);
	printf("%d", fact13);
}
