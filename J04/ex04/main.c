/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: igradea <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/03 21:44:52 by igradea           #+#    #+#             */
/*   Updated: 2016/09/03 21:49:36 by igradea          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	ft_fibonacci(int index);

int	main(void)
{
	int fib0;
	int fib1;
	int fib4;
	int fib12;
	int fib_1;
	
	fib0 = ft_fibonacci(0);
	fib1 = ft_fibonacci(1);
	fib4 = ft_fibonacci(4);
	fib12 = ft_fibonacci(12);
	fib_1 = ft_fibonacci(-1);
	printf("%d", fib0);
	printf("%d", fib1);
	printf("%d", fib4);
	printf("%d", fib12);
	printf("%d", fib_1);
}
