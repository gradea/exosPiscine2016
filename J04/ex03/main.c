/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: igradea <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/03 17:36:48 by igradea           #+#    #+#             */
/*   Updated: 2016/09/03 21:39:26 by igradea          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_recursive_power(int nb, int power);

int		main(void)
{
	int iter23;
	int iter45;
	int iter2_10;
	int iter80;

	iter23 = ft_recursive_power(2,3);
	iter45 = ft_recursive_power(4,5);
	iter2_10 = ft_recursive_power(2, -10);
	iter80 = ft_recursive_power(8,0);
	printf("%d - ",iter23);
	printf("%d - ",iter45);
	printf("%d - ",iter2_10);
	printf("%d", iter80);
}
