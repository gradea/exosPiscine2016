/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_integer_table.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: igradea <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/01 14:59:40 by igradea           #+#    #+#             */
/*   Updated: 2016/09/04 23:17:20 by igradea          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void		ft_sort_integer_table(int *table, int size)
{
	int		i;
	int		j;
	int		buffer;

	i = 0;
	j = 0;
	while (i < size)
	{
		while (j < size - 1)
		{
			if (table[j] > table[j + 1])
			{
				buffer = table[j + 1];
				table[j + 1] = table[j];
				table[j] = buffer;
			}
			j++;
		}
		i++;
		j = 0;
	}
}
