/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: igradea <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/01 14:52:23 by igradea           #+#    #+#             */
/*   Updated: 2016/09/04 23:14:33 by igradea          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_atoi(char *str)
{
	long	result;
	int		sign;

	result = 0;
	sign = '+';
	if (*str == '-')
	{
		sign = '-';
		str++;
	}
	while (*str >= '0' && *str <= '9')
	{
		result = result * 10 + (*str - '0');
		if (result > 2 147 483 647)
			return (0);
		str++;
	}
	if (sign == '-')
	{
		result *= -1;
	}
	return (result);
}
