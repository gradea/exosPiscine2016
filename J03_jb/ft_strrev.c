/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: igradea <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/01 12:46:37 by igradea           #+#    #+#             */
/*   Updated: 2016/09/04 23:18:29 by igradea          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char			*ft_strrev(char *str)
{
	int			i;
	int			j;
	char		buffer_char;

	i = 0;
	j = 0;
	while (str[j])
	{
		j++;
	}
	j--;
	while (i < j)
	{
		buffer_char = str[i];
		str[i] = str[j];
		str[j] = buffer_char;
		i++;
		j--;
	}
	return (str);
}
