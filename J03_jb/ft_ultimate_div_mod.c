/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_div_mod.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: igradea <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/01 11:58:08 by igradea           #+#    #+#             */
/*   Updated: 2016/09/04 23:19:52 by igradea          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void		ft_ultimate_div_mod(int *a, int *b)
{
	int		buffer_div;
	int		buffer_mod;

	buffer_div = *a / *b;
	buffer_mod = *a % *b;
	*a = buffer_div;
	*b = buffer_mod;
}
