/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: igradea <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/06 23:24:57 by igradea           #+#    #+#             */
/*   Updated: 2016/09/06 23:30:13 by igradea          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_is_alphanum(char c)
{
	if (c >= '0' && c <= '9')
		return (1);
	else if (c >= 'a' && c <= 'z')
		return (1);
	else if (c >= 'A' && c <= 'Z')
		return (1);
	return (0);
}

char	*ft_strcapitalize(char *str)
{
	char *s;

	s = str;
	if (ft_is_alphanum(*s) && *s >= 'a' && *s <= 'z')
		*s -= 32;
	s++;
	while (*s != '\0')
	{
		if (ft_is_alphanum(*(s - 1)) && *s >= 'A' && *s <= 'Z')
			*s += 32;
		if (!ft_is_alphanum(*(s - 1)) && *s >= 'a' && *s <= 'z')
			*s -= 32;
		s++;
	}
	return (str);
}
