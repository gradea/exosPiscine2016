/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: igradea <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/05 17:05:53 by igradea           #+#    #+#             */
/*   Updated: 2016/09/05 17:46:45 by igradea          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putstr(char *str);
void	ft_putchar(char c);

int		main(void)
{
	ft_putstr("abcd");
	ft_putchar('\n');
	ft_putstr("123ds");
	ft_putchar('\n');
	ft_putstr("wqewqe12");
	return (0);
}
