/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: igradea <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/06 14:37:29 by igradea           #+#    #+#             */
/*   Updated: 2016/09/06 18:12:02 by igradea          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int	ft_atoi(char *str);

int	main(void)
{
	printf("\natoi - 123abc : %d\n", atoi("123abc"));
	printf("\natoi - def123abc: %d\n", atoi("def123abc"));
	printf("\natoi - 12356090-00abc456: %d\n", atoi("12356090-00abc456"));
	printf("\natoi - xxx123abc963 : %d\n",atoi("xxx-123abc963"));
	printf("\natoi - -122xxx123abc963 : %d\n", atoi("-122xxx123abc963"));

	printf("\natoi - over + / 2147483648abc : %d\n", atoi("2147483648abc")); 
	printf("\natoi - over - / -2147483649abc : %d\n", atoi("-2147483649abc")); 

	printf("\nft_atoi - 123abc : %d\n", ft_atoi("123abc"));
	printf("\nft_atoi - def123abc: %d\n", ft_atoi("def123abc"));
	printf("\nft_atoi - 12356090-00abc456: %d\n", ft_atoi("12356090-00abc456"));
	printf("\nft_atoi - xxx123abc963 : %d\n",ft_atoi("xxx-123abc963"));
	printf("\nft_atoi - -122xxx123abc963 : %d\n", ft_atoi("-122xxx123abc963"));

	printf("\nft_atoi - over + / 2147483648abc : %d\n", ft_atoi("2147483648abc")); 
	printf("\nft_atoi - over - / -2147483649abc : %d\n", ft_atoi("-2147483649abc")); 
	return (0);
}
