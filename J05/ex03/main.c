/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: igradea <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/06 18:16:42 by igradea           #+#    #+#             */
/*   Updated: 2016/09/06 18:35:04 by igradea          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int	main(void)
{
	printf("\nstrcpy - abc123- : %s\n", strcpy("", "abc123-"));
	printf("\nstrcpy - ab32c123y- : %s\n", strcpy("", "ab32c123y-"));
	printf("\nstrcpy - xxxabc123- : %s\n", strcpy("", "xxxabc123-")); 
}

