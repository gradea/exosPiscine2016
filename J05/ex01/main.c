/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: igradea <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/05 17:05:53 by igradea           #+#    #+#             */
/*   Updated: 2016/09/06 17:35:59 by igradea          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

void	ft_putchar(char c);
void	ft_putnbr(int nb);

int		main(void)
{
	printf("\nPositif int : \n");
	ft_putnbr(2147483647);
	ft_putchar('\n');
	ft_putnbr(2147483649);
	
	printf("\nNegatif int : \n");
	ft_putnbr(-2147483647);
	ft_putchar('\n');
	ft_putnbr(-2147483648);
	ft_putnbr(-2147483649);
	ft_putchar('\n');

ft_putchar('\n');
	ft_putnbr(1234);
    ft_putchar('\n');
	ft_putnbr(16553);
	ft_putchar('\n');
	ft_putnbr(42);
return (0);
}
