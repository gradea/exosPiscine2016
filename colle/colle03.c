/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colle03.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jberthie <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/02 23:02:37 by jberthie          #+#    #+#             */
/*   Updated: 2016/09/04 22:17:12 by jberthie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar(char c);

void	ft_print_line(char start, char mid, char end, int column_total)
{
	int		column_middle;

	column_middle = 0;
	ft_putchar(start);
	if (column_total == 2)
		ft_putchar(end);
	else if (column_total >= 3)
	{
		column_middle = column_total - 2;
		while (column_middle > 0)
		{
			ft_putchar(mid);
			column_middle--;
		}
		ft_putchar(end);
	}
	ft_putchar('\n');
}

void	ft_print_first_line(int column_total)
{
	ft_print_line('A', 'B', 'C', column_total);
}

void	ft_print_middle_lines(int column_total, int raw_total)
{
	int		raw_middle;

	raw_middle = raw_total - 2;
	while (raw_middle > 0)
	{
		ft_print_line('B', ' ', 'B', column_total);
		raw_middle--;
	}
}

void	ft_print_last_line(int column_total)
{
	ft_print_line('A', 'B', 'C', column_total);
}

int		colle(int x, int y)
{
	int		column_total;
	int		raw_total;

	column_total = x;
	raw_total = y;
	if (column_total <= 0 || raw_total <= 0)
		return (-1);
	if (raw_total >= 1)
		ft_print_first_line(column_total);
	if (raw_total >= 3)
		ft_print_middle_lines(column_total, raw_total);
	if (raw_total >= 2)
		ft_print_last_line(column_total);
	return (0);
}
