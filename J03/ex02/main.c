/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: igradea <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/02 23:14:39 by igradea           #+#    #+#             */
/*   Updated: 2016/09/02 23:20:46 by igradea          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_swap(int *a, int *b);

int		main(void)
{
	int a;
	int b;
	int *ptr_a;
	int *ptr_b;

	ptr_a = &a;
	ptr_b = &b;
	a = 1;
	b = 2;

	ptrinf("%i", a);
	ptrinf("%i", b);

	ft_swap(ptr_a, ptr_b);

	ptrinf("%i", a);
	ptrinf("%i", b);
}
